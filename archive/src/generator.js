function makeBaseLayer(maxZoom, theme, MAPBOX_TOKEN) {
    return L.tileLayer(
        //'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=' + MAPBOX_TOKEN,
        "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
        {
            maxZoom: maxZoom,
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',

            // attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            //     '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            //     'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            id: theme,
            // tileSize: 512,
            // zoomOffset: -1
        })
}

function makeGeoLayerJSON(geodata, graphData, min, max, allStyles, colorScale, onEachFeature, infoBox, bezierOffset) {
    return L.geoJson(geodata, {
        style: (a) => style(a, min, max, graphData, allStyles, colorScale, bezierOffset),
        onEachFeature: (feature, layer) => onEachFeature(feature, layer, allStyles.hover, infoBox)
    })
}

function makeInfoBox(title, graphData, regionName, dataName) {
    // control that shows state info on hover
    let info = L.control();

    info.onAdd = function (map) {
        this._div = L.DomUtil.create('div', 'info');
        this.update();
        return this._div;
    };

    info.update = function (props) {
        const titleHTML = title ? `<h4>${title}</h4>` : ""
        const labelHTML = props ? `<b>${regionName}: ${props.name}</b><br/>
                                      ${dataName}: ${graphData[props.name]}` : 'Hover over a region'
        this._div.innerHTML = `${titleHTML}<div>${labelHTML}</div>`
    };

    return info;
}


function makeLegend(round, numColors, colorScale, bezierOffset, min, max) {
    const legend = L.control({position: 'bottomright'});

    legend.onAdd = function (map) {

        let div = L.DomUtil.create('div', 'info legend')

        // space between legend markings
        let diff = Math.round((max - min) / numColors)

        let grades = [...Array(numColors + 1).keys()].map(i => (
            Math.round((min + i * diff + 1) / round) * round
        ))

        //generate html based on grades array
        let labels = grades.map(grade => '<i style="background:' + getColor(grade, min, max, colorScale, bezierOffset) + '"></i> ' + grade)
        div.innerHTML = labels.join('<br>');

        return div;
    };

    return legend
}