let map;

function makeMap(model, defaultModel) {
    //if map already exists, destroy it
    if (map) map = map.remove()
    map = L.map('map')

    if (!validateModel(model)) throw new Error("Model object must contain keys 'locations', and 'data'")

    //this is the merged model
    const MODEL = $.extend(true, defaultModel, model)
    const {locations, data} = MODEL

    //turn locations and data array into a dictionary
    const graphData = {}
    locations.forEach((location, i) => graphData[location] = data[i]);

    const min = Math.min(...data), max = Math.max(...data)

    if(!MODEL.legend.round){
        MODEL.legend.round = autoRound(min, max)
    }

    const baseLayer = makeBaseLayer(MODEL.map.maxZoom, MODEL.map.theme)
    const infoBox = makeInfoBox(MODEL.title, graphData, MODEL.regionName, MODEL.dataName)
    const legend = makeLegend(MODEL.legend.round, MODEL.legend.n - 1, MODEL.colorScale, MODEL.bezierOffset, min, max);

    [baseLayer, infoBox, legend].forEach(x => x.addTo(map))


    const locationKeys = Object.keys(graphData)
    const urlParams = locationKeys.map(loc => "key=" + loc).join("&")

    if(locationKeys.length === 0) return

    //'https://mokshadata.gitlab.io/retool-choropleth-mapper/geojson/' + MODEL.geoJSON + '.json'

    return $.get( `https://geojson.mokshadata.com/${MODEL.geoJSON}?property=${MODEL.property}&${urlParams}`)
        .then((geodata) => {
            return geodata
        })
        .then((geodata) => {
            const geoJSONLayer = makeGeoLayerJSON(geodata, graphData, min, max, MODEL.style, MODEL.colorScale, onEachFeature, infoBox, MODEL.bezierOffset)
            geoJSONLayer.addTo(map)

            if(MODEL.map.fitBounds) map = map.fitBounds(MODEL.map.fitBounds)
            else map = map.fitBounds(geoJSONLayer.getBounds())

        })
        .catch((error) => {
            console.warn(error)
        })
}
