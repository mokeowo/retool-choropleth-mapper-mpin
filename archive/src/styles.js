const getColor = (d, min, max, colorScale, bezierOffset) => (
    adaptedColorScale(((d - min) / (max - min)), d3[colorScale], bezierOffset)
)

//styling function
const style = (feature, min, max, graphData, styles, colorScale, bezierOffset) => ({
    fillColor: getColor(graphData[feature.properties.name], min, max, colorScale, bezierOffset),
    ...styles,
});

const adaptedColorScale = (n, scalingFunction, bezierArgs) => {
    if(n < 0) n = 0
    if(n > 1) n = 1

    //bezierArgs
    const easing = BezierEasing(...bezierArgs)
    return scalingFunction(easing(n))
}

//mouseover
const highlightFeature = (e, hoverStyles, infoBox) => {
    const layer = e.target;
    layer.setStyle(hoverStyles);

    if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();
    }
    infoBox.update(layer.feature.properties);
}

//mouseout
const resetHighlight = (e, infoBox) => {
    const layer = e.target;
    const parentLayers = layer._eventParents;
    const parentKeys = Object.keys(parentLayers)
    parentKeys.forEach((layerKey) => {
        parentLayers[layerKey].resetStyle();
    })
    infoBox.update();
}

const zoomToFeature = (e) => map.fitBounds(e.target.getBounds(), {
    paddingTopLeft: L.point(32, 32),
    paddingBottomRight: L.point(128, 64)
})

const onEachFeature = (feature, layer, hoverStyles, infoBox) => (
    layer.on({
        mouseover: (e) => highlightFeature(e, hoverStyles, infoBox),
        mouseout: (e) => resetHighlight(e, infoBox),
        click: (e) => {
            console.log(e)
            window.Retool.modelUpdate({selected: e.target.feature.properties.name})
            zoomToFeature(e)
        }
    })
)
