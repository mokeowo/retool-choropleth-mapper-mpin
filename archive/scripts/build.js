// const Inliner = require('inliner');
const fs = require('fs');
const inline = require("web-resource-inliner");

const Terser = require("terser");
const CleanCSS = require('clean-css');


const readFile = (file) => normalize(fs.readFileSync(file, "utf8"))
const normalize = (contents) => process.platform === "win32" ? contents.replace(/\r\n/g, "\n") : contents

function minifyCode(content, callback) {
    const {error, code} = Terser.minify(content.toString())
    if (error) return console.warn(error)
    callback(error, code)
    return code
}

function minifyStyles(content, callback) {
    var {styles, errors} = new CleanCSS().minify(content);
    if (errors.length > 0) return console.warn(errors)
    callback(errors[0], styles)
    return styles
}

const SOURCE_PATH = "../src/index.html"
const DESTINATION_PATH = "../dist/index.html"

function getDirectory(path){
    while(path.slice(-1) !== '/') path = path.slice(0, -1);
    return path
}

inline.html({
        fileContent: readFile(SOURCE_PATH),
        relativeTo: getDirectory("../src/"),
        //scriptTransform: minifyCode,
        linkTransform: minifyStyles,
        scripts: false,
        links: false,
        images: false,
        svgs: false,
    }, function (err, html) {

        if (err) return console.log(err);

        fs.writeFile(DESTINATION_PATH, html, function (err) {
            if (err) return console.log(err);
            console.log(`The file was saved to ${DESTINATION_PATH}!`);
        });
    }
);
