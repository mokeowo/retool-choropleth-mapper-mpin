const express = require('express');
const fs = require('fs');

const app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    next();
});

const files = {}

const getFile = (fileName) => {
    const memFile = files[fileName]
    if(memFile) return memFile
    else{
        const geo = JSON.parse(fs.readFileSync(fileName, 'utf8'));
        files[fileName] = geo
        return geo
    }
}

const defaultFile = 'dolu_tx_texas_county_mpin.geojson'

app.get('/:file', (req, res) => {

    const fileName = req.params.file || defaultFile
    const keys = req.query.key
    const property = req.query.property || "NAME"

    const geo = getFile(fileName);

    const filteredFeatures = keys ? geo.features.filter((f) => keys.includes(f.properties[property])) : geo.features

    const addName = filteredFeatures.map(f => {
        f.properties.name = f.properties[property]
        return f
    })

    const filteredGeo = {type : "FeatureCollection", features: addName}

    res.send(filteredGeo)
});

app.get('/', (req, res) => {
    res.send("GET /:fileName for geoJSON");
})

const PORT = process.env.PORT || 8000;

app.listen(PORT, () => {
    console.log(`server started on port ${PORT}`);
});
