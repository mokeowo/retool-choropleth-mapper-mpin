export function validateModel(model) {
    const keys = Object.keys(model)
    const required = ['locations', 'data']

    return required.every(key => keys.includes(key)) &&
        validateInput(model.locations, model.data)
}

// locations array and data array
const validateInput = (locations, data) => (
    locations && data && locations.length === data.length && data.every(x => !isNaN(x))
)

//check if the model changed
export function modelChanged(model, prevModel){
    const ignoredKeys = ['selected']

    let modelCopy = copyObject(model)
    let prevModelCopy = copyObject(prevModel)

    ignoredKeys.forEach(function(key){
        if (prevModelCopy[key]) delete prevModelCopy[key]
        if (modelCopy[key]) delete modelCopy[key]
    })
    console.log(prevModelCopy)
    console.log(modelCopy)
    return JSON.stringify(prevModelCopy) === JSON.stringify(modelCopy)

}

// this doesn't seem to work for some reason
//const copyObject = (obj) => $.extend({}, obj)

//this approach comes with some caveats, but seems to work better
export const copyObject = (obj) => JSON.parse(JSON.stringify(obj))

export const autoRound = (min, max) => Math.pow(10, Math.round(Math.log10(max-min)-2))