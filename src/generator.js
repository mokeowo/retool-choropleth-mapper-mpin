import {style, getColor} from './styles'

export function makeBaseLayer(maxZoom, theme) {
    return L.tileLayer(
        //'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=' + MAPBOX_TOKEN,
        "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
        {
            maxZoom: maxZoom,
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',

            // attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            //     '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            //     'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            id: theme,
            // tileSize: 512,
            // zoomOffset: -1
        })
}

export function makeGeoLayerJSON(geodata, graphData, min, max, allStyles, colorScale, onEachFeature, infoBox, bezierOffset, map) {
    return L.geoJson(geodata, {
        style: (a) => style(a, min, max, graphData, allStyles, colorScale, bezierOffset),
        onEachFeature: (feature, layer) => onEachFeature(feature, layer, allStyles.hover, infoBox, map)
    })
}

export function makeInfoBox(title, graphData, regionName, dataName, noData = false) {
    // control that shows state info on hover
    let info = L.control();

    info.onAdd = function (map) {
        this._div = L.DomUtil.create('div', 'info');
        noData ? this._div.innerHTML = "No data available" : this.update();
        return this._div;
    };

    if (!noData) {
        info.update = function (props) {
            const titleHTML = title ? `<h4>${title}</h4>` : ""
            const labelHTML = props ? `<b>${regionName}: ${props.name}</b><br/>
                                      ${dataName}: ${graphData[props.name]}` : 'Hover over a region'
            this._div.innerHTML = `${titleHTML}<div>${labelHTML}</div>`
        };
    } else {
        info.update = () => {
        }
    }

    return info;
}


export function makeLegend(round, numColors, colorScale, bezierOffset, min, max) {
    const legend = L.control({position: 'bottomright'});

    legend.onAdd = function (map) {

        let div = L.DomUtil.create('div', 'info legend')
        let grades;

        const range = max - min

        if (range <= numColors) {
            grades = [...Array(range + 1).keys()].map(x => x + min)
        } else {
            // space between legend markings
            let diff = Math.round(range / numColors)

            grades = [...Array(numColors + 1).keys()].map(i => {
                const val = Math.round((min + i * diff ) / round) * round
                // if val is 0, force return 1
                return val === 0 ? 1 : val
            })
        }

        //generate html based on grades array
        let labels = grades.map(grade => '<i style="background:' + getColor(grade, min, max, colorScale, bezierOffset) + '"></i> ' + grade)
        div.innerHTML = labels.join('<br>');

        return div;
    };

    return legend
}