const $ = require("jquery");
const L = require('leaflet');

const {onEachFeature} = require('./styles');
const {makeBaseLayer, makeInfoBox, makeLegend, makeGeoLayerJSON} = require('./generator')
const {validateModel, autoRound, modelChanged, copyObject} = require('./utils')

const DEFAULT_MODEL = {
    "property": "NAME",
    "geoJSON": "dolu_tx_texas_county_mpin.geojson",
    "bezierOffset": [0, 0, 1, 1],
    "regionName": "Region",
    "dataName": "Data",
    "map": {
        "maxZoom": 18,
        "theme": "mapbox/light-v9",
    },
    "colorScale": "interpolateYlGnBu",
    "legend": {
        "n": 8,
    },
    "style": {
        "weight": 0.6,
        "opacity": 1,
        "color": "black",
        "fillOpacity": 0.6,
        "hover": {
            "weight": 3,
            "color": "darkred",
            "fillOpacity": 0.75
        }
    }
}

let map;

function makeMap(model, defaultModel = copyObject(DEFAULT_MODEL), mapId = 'map') {
    //if map already exists, destroy it
    if (map) map = map.remove()
    map = L.map(mapId)

    if (!validateModel(model)) throw new Error("Model object must contain keys 'locations', and 'data'")

    //this is the merged model
    const MODEL = $.extend(true, defaultModel, model)
    const {locations, data} = MODEL

    //turn locations and data array into a dictionary
    const graphData = {}
    locations.forEach((location, i) => graphData[location] = data[i]);

    const min = Math.min(...data), max = Math.max(...data)

    // if rounding not specified, employ autorounding
    if (!MODEL.legend.round) {
        MODEL.legend.round = autoRound(min, max)
    }

    const noData = locations.length === 0

    //start building the map
    const baseLayer = makeBaseLayer(MODEL.map.maxZoom, MODEL.map.theme).addTo(map)
    const infoBox = makeInfoBox(MODEL.title, graphData, MODEL.regionName, MODEL.dataName, noData).addTo(map)

    if (noData) {
        if (MODEL.map.fitBounds) map = map.fitBounds(MODEL.map.fitBounds)
        return
    }

    const legend = makeLegend(MODEL.legend.round, MODEL.legend.n - 1, MODEL.colorScale, MODEL.bezierOffset, min, max);
    legend.addTo(map)

    // make url params from location list
    const urlParams = locations.map(loc => "key=" + loc).join("&")

    //'https://mokshadata.gitlab.io/retool-choropleth-mapper/geojson/' + MODEL.geoJSON + '.json'
    $.get(`https://geojson.mokshadata.com/${MODEL.geoJSON}?property=${MODEL.property}&${urlParams}`)
        .then((geodata) => {
            return geodata
        })
        .then((geodata) => {
            const geoJSONLayer = makeGeoLayerJSON(geodata, graphData, min, max, MODEL.style, MODEL.colorScale, onEachFeature, infoBox, MODEL.bezierOffset, map)
            geoJSONLayer.addTo(map)

            if (MODEL.map.fitBounds) map = map.fitBounds(MODEL.map.fitBounds)
            else map = map.fitBounds(geoJSONLayer.getBounds())

        })
        .catch((error) => {
            console.warn(error)
        })

    return map
}

function addMarkers(points, scale= 1.4){
    if (points) {
        points.forEach(point => {
            const {loc, name} = point
            if(loc){
                const marker = L.marker(loc)
                if(name) marker.bindPopup(point.name)

                const {options} = marker.options.icon;
                const downscale = (arr, n) => arr ? arr.map(x => x/n) : null
                const {iconSize, shadowSize, iconAnchor, shadowAnchor, popupAnchor} = L.Icon.Default.prototype.options;

                options.iconSize = downscale(iconSize, scale)
                options.shadowSize = downscale(shadowSize, scale)
                options.iconAnchor = downscale(iconAnchor, scale)
                options.shadowAnchor = downscale(shadowAnchor, scale)
                options.popupAnchor = downscale(popupAnchor, scale)

                marker.addTo(map)
            }
        })
    }
}


function addCircles(points){
    if (points) {
        points.forEach(point => {
            const {loc, name, options} = point
            if(loc){
                const defaults = {
                    color: "red",
                    fillColor: "#f03",
                    fillOpacity: 0.5,
                    radius: 50.0
                }
                const marker = L.circle(loc, options || defaults);
                // const marker = L.marker(loc)
                if(name) marker.bindPopup(point.name)
                marker.addTo(map)
            }
        })
    }
}


module.exports = {
    makeMap,
    addMarkers,
    addCircles,
    utils: {
        modelChanged: modelChanged,
        naiveCopyObject: copyObject,
    },
}
