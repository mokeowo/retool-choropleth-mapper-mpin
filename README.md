# Retool Choropleth Mapper
Provide the ability to graph choropleths in retool with leaflet.js

Live demo at https://mokshadata.gitlab.io/retool-choropleth-mapper

## Getting Started
Copy over the HTML from [public/index.html](public/index.html) 
to the retool component. Just make sure to replace:

`<script src="main.js"></script>`  with

`<script src="https://mokshadata.gitlab.io/retool-choropleth-mapper/main.js"></script>`.

## Model Parameters
**Name**|**Type**|**Default**|**Description**
-----|-----|-----|-----
locations|array| |list of location names, corresponding to properties.name in the geoJSON (from retool)
data|array (numbers)| |list of numerical data corresponding to locations list (from retool)
title|text| |Title of the graph, displayed in the info panel
geoJSON|text|`'tx_texas_zip_codes_geo.min.json'`|geoJSON file name (only one file right now)
map.theme|text|`'mapbox/light-v9'`|mapbox theme id (switched to free tiling for now, so not relevant)
map.maxZoom|number|`18`|max zoom level for map
map.fitBounds|2D array| |Top left and bottom right point to fit bounds to. Default behavior is to fit all features. ex: `[[30.2875, -94.4768],[29.2648, -96.3747]]`
colorScale|text|`'interpolateYlGnBu'`|name of [d3-scale-chromatic](https://github.com/d3/d3-scale-chromatic) color scaling function
legend.n|number|`8`|Displays n+1 numbers in the legend
legend.round|number| |Rounds legend markings to the nearest n, default behavior is auto rounding depending on data range
style.weight|number|`0.6`|Region border weight
style.opacity|number|`1`|Region border opacity
style.color|text|`'black'`|Region border color
style.fillOpacity|number|`0.6`|Region fill opacity
style.hover.weight|number|`5`|Region border weight on hover
style.hover.color|text|`'#666'`|Region border color on hover
style.hover.fillOpacity|number|`0.7`|Region fill opacity on hover
property|text|`'ZCTA5CE10'`|Property name that has region identifier
regionName|text|`'Region'`|Region name to display in info box
dataName|text|`'Data'`|Data name to display in info box
bezierOffset|number array|`[0, 0, 1, 1]`|[cubic-bezier](https://cubic-bezier.com/) offset to apply to color scaling function

**Note:** Other styling params might work, but haven't been tested yet. Here's the [full list from the leaflet.js docs.](https://leafletjs.com/reference-1.6.0.html#path-option)
### Example (a bit outdated)
```
{
    "title":"Population by Zip Code",
    "locations": {{zip_counts_hfb.data['zip code']}},
    "data": {{zip_counts_hfb.data.count}},
    "geoJSON": "zip_code",
    "map":{
        "theme": "mapbox/dark-v9",
        "maxZoom": 18
    },
    "colorScale": "interpolateOrRd",
    "legend":{
        "n": 5,
        "round": 100
    },
    "style":{
        "weight": 1,
        "opacity": 0.7,
        "color": "purple",
        "fillOpacity": 0.3,
        "hover":{
            "weight": 2,
            "color": "blue",
            "fillOpacity": 0.8
        }
    }
}
```

![Example image](example.PNG)